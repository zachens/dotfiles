    -- Main Imports --

import qualified XMonad.StackSet as W
import XMonad.ManageHook
import XMonad (appName)
import Data.Monoid
import System.Exit
import XMonad

    -- Layout Imports --

import XMonad.Layout.Renamed (renamed, Rename(Replace))
import XMonad.Layout.LayoutModifier
import XMonad.Layout.GridVariants
import XMonad.Hooks.ManageDocks
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing

    -- Util Imports --

import XMonad.Util.NamedScratchpad
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig

    -- Custom Variables --

myTerminal :: String
myTerminal = "alacritty"

myBrowser :: String
myBrowser = "brave"

myScripts :: String
myScripts = "~/.local/bin/"

    -- BuiltIn Variables --

myFocusedBorderColor :: String
myFocusedBorderColor = "#4b4b4b"

myNormalBorderColor :: String
myNormalBorderColor = "#282c34"

myBorderWidth :: Dimension
myBorderWidth = 5

myStartupHook :: X ()
myStartupHook = do
    spawnOnce "hsetroot -cover ~/dox/pix/wp/31.jpg &"
    spawnOnce "tmux new-session -d -s\983626 &"
    spawnOnce "xset r rate 300 50 &"
    spawnOnce "unclutter &"
    spawnOnce "picom &"

myManageHook = composeAll
    [] <+> namedScratchpadManageHook myScratchPads

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "spotify" spawnSpot findSpot manageSpot
                , NS "terminal" spawnTerm findTerm manageTerm
                ]
  where
    spawnSpot  = myTerminal ++ " --class spotify -e spotify_player"
    findSpot   = appName =? "spotify"
    manageSpot = customFloating $ W.RationalRect l t w h
               where
                 h = 0.6
                 w = 0.6
                 t = 0.8 -h
                 l = 0.8 -w
    
    spawnTerm = myTerminal ++ " --class terminal -e bash"
    findTerm = appName =? "terminal"
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.6
                 w = 0.6
                 t = 0.8 -h
                 l = 0.8 -w

myLayoutHook = avoidStruts (grid ||| full)
    where

        full = renamed [Replace "Full"]
             $ noBorders (Full)

        grid = renamed [Replace "Grid"]
             $ spacingRaw False (Border 10 0 10 0) True (Border 0 10 0 10) True
             $ Grid (16/10)

myWorkspaces = ["dev","web", "mus","dox"]

myModMask = mod4Mask

myKeys :: [(String, X ())]
myKeys =

    -- ScratchPads
    [ ("M-S-s", namedScratchpadAction myScratchPads "spotify")
    , ("M-\\", namedScratchpadAction myScratchPads "terminal")

    -- Applications
    , ("M-S-z", spawn (myTerminal ++ " -e tmux attach"))
    , ("M-S-a", spawn (myBrowser)) 
    , ("M-p", spawn "dmenu_run")

    -- Scripts
    , ("M-c", spawn (myScripts ++ "x11Screenshot"))
    , ("M-s", spawn (myScripts ++ "engineSearch"))
    , ("M-a", spawn (myScripts ++ "animePicker"))
    , ("M-f", spawn (myScripts ++ "x11Capture"))
    , ("M-z", spawn (myScripts ++ "acpiEvents"))
    , ("M-S-t", spawn (myScripts ++ "todoDmenu"))
    , ("M-x", spawn (myScripts ++ "pidKill"))
    ]

    -- Main Function --

main = xmonad defaults
defaults = def {
    focusedBorderColor = myFocusedBorderColor,
    normalBorderColor  = myNormalBorderColor,
    borderWidth        = myBorderWidth,
    startupHook        = myStartupHook,
    manageHook         = myManageHook,
    layoutHook         = myLayoutHook,
    workspaces         = myWorkspaces,
    terminal           = myTerminal,
    modMask            = myModMask
} `additionalKeysP` myKeys
