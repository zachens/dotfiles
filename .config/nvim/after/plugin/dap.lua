local dap = require('dap')
dap.adapters.lldb = {
  type = 'executable',
  name = 'lldb',
  command = '~/.local/share/nvim/mason/packages/codelldb/extension/adapter/libcodelldb.so',
}

dap.configurations.cpp = {
  {
    name = "Launch",
    type = "lldb",
    request = "launch",
    program = function()
      return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
    cwd = '${workspaceFolder}',
    stopOnEntry = false,
    args = {},
  },
}

dap.configurations.c = dap.configurations.cpp
dap.configurations.rust = dap.configurations.cpp
