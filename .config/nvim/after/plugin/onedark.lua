require("onedarkpro").setup({
  colors = {
      bg = "#2b2b2b",
      float_bg = "#333333",
      bg_statusline = "#333333",
  },
  plugins = {
      telescope = true,
      treesitter = true,
  },
  filetypes = {
      lua = true,
      rust = true,
      comment = true,
  },
  options = {
      cursorline = false,
      transparency = false,
      terminal_colors = true,
      highlight_inactive_windows = false,
  },
})

vim.cmd("colorscheme onedark")
