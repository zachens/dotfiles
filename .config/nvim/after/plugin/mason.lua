require("mason").setup({
    ui = {
        icons = {
            package_installed = "",
            package_pending = "",
            package_uninstalled = "",
        },
        width = 0.4,
        height = 0.7,
        border = "rounded",
    },
})
require("mason-lspconfig").setup()
