local lualine = require('lualine')

local colors = {
  bg1      = '#4b4b4b',
  bg       = '#2b2b2b',
  fg       = '#abb2bf',
  yellow   = '#ECBE7B',
  cyan     = '#008080',
  darkblue = '#081633',
  green    = '#98be65',
  orange   = '#FF8800',
  violet   = '#a9a1e1',
  magenta  = '#c678dd',
  blue     = '#51afef',
  red      = '#ec5f67',
}

local conditions = {
  buffer_not_empty = function()
    return vim.fn.empty(vim.fn.expand('%:t')) ~= 1
  end,
  hide_in_width = function()
    return vim.fn.winwidth(0) > 80
  end,
  check_git_workspace = function()
    local filepath = vim.fn.expand('%:p:h')
    local gitdir = vim.fn.finddir('.git', filepath .. ';')
    return gitdir and #gitdir > 0 and #gitdir < #filepath
  end,
}

local config = {
  options = {
    icons_enabled = true,
    component_separators = '',
    section_separators = '',
    globalstatus= true,
  },
  sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_y = {},
    lualine_z = {},
    lualine_c = {},
    lualine_x = {},
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_y = {},
    lualine_z = {},
    lualine_c = {},
    lualine_x = {},
  },
}

local function ins_left(component)
  table.insert(config.sections.lualine_c, component)
end

local function ins_right(component)
  table.insert(config.sections.lualine_x, component)
end

ins_left {
  function()
    return '▊'
  end,
  color = { fg = colors.bg1 },
  padding = { left = 0, right = 1 },
}
 
ins_left {
  function()
    return '󰉊'-- 󰏅, 󰌪, 󱜿, 󰏒, 󱢺, 󰈺, 󰈄, 󰊠, 󰉊,
  end,
  color = function()
    local mode_color = {
      n = colors.red,
      i = colors.green,
      v = colors.magenta,
      [''] = colors.magenta,
      V = colors.magenta,
      c = colors.magenta,
      no = colors.red,
      s = colors.orange,
      S = colors.orange,
      [''] = colors.orange,
      ic = colors.yellow,
      R = colors.violet,
      Rv = colors.violet,
      cv = colors.red,
      ce = colors.red,
      r = colors.cyan,
      rm = colors.cyan,
      ['r?'] = colors.cyan,
      ['!'] = colors.red,
      t = colors.red,
    }
    return { fg = mode_color[vim.fn.mode()] }
  end,
  padding = { right = 1 },
}

ins_left {
  -- filesize component
  'filesize',
  cond = conditions.buffer_not_empty,
  padding = { left = 1, right = 0 },
}

ins_left {
 'filename',
  cond = conditions.buffer_not_empty,
  color = { fg = colors.magenta, gui = '' },
  path = 1,
}

ins_left {
    'diagnostics',
    sources = { 'nvim_diagnostic', 'nvim_lsp' },
    colored = true,
    symbols = { error = ' ', warn = ' ', info = ' ', hint = '󰓏 ' },
    padding = { left = 1, right = 1 },
    always_visible = false,
    diagnostics_color = {
        color_error = { bg = "#2b2b2b", fg = "#ec5f67" },
        color_warn = { bg = "#2b2b2b", fg = "#ECBE7B" },
        color_info = { bg = '#2b2b2b', fg = "#51afef" },
        color_hint = { bg = '#2b2b2b', fg = '#98be65' },
    },
}

ins_right {
    'branch',
    icon = '',
    color = { fg = colors.red },
    padding = { left = 1, right = 1 },
}

ins_right {
    'diff',
    symbols = { added = ' ', removed = ' ', modified = '󱓼 ' },
    padding = { left = 0, right = 1 },
    diff_color = {
        added = { fg = '#98be65' },
        removed = { fg = '#ec5f67' },
        modified = { fg = '#ff8800' },
    },
    cond = conditions.hide_in_width,
}

ins_right {
  function()
    local msg = 'unknown'
    local buf_ft = vim.api.nvim_buf_get_option(0, 'filetype')
    local clients = vim.lsp.get_active_clients()
    if next(clients) == nil then
      return msg
    end
    for _, client in ipairs(clients) do
      local filetypes = client.config.filetypes
      if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 then
        return client.name
      end
    end
    return msg
  end,
  icon = '',
  color = { fg = colors.magenta },
  padding = { left = 0, right = 1 },
}

ins_right { 
    'location', 
    color = { fg = "#abb2bf" },
    padding = {
        right = 1,
        left = 0,
    }
}
  
ins_right {
  function()
    return '▊'
  end,
  color = { fg = colors.bg1 },
  padding = { left = 0, right = 0 },
}

lualine.setup(config)
