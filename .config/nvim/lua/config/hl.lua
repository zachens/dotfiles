-- DIAGNOSTIC HIGHLIGHTS
vim.api.nvim_set_hl(0, "DiagnosticError", {bg="#2b2b2b", fg="#ec5f67"})
vim.api.nvim_set_hl(0, "DiagnosticWarn", {bg="#2b2b2b", fg="#ECBE7B"})
vim.api.nvim_set_hl(0, "DiagnosticInfo", {bg="#2b2b2b", fg="#51afef"})
vim.api.nvim_set_hl(0, "DiagnosticHint", {bg="#2b2b2b", fg="#98be65"})
vim.api.nvim_set_hl(0, "DiagnosticFloatingError", {bg="#2b2b2b", fg="#ec5f67"})
vim.api.nvim_set_hl(0, "DiagnosticFloatingWarn", {bg="#2b2b2b", fg="#ECBE7B"})
vim.api.nvim_set_hl(0, "DiagnosticFloatingInfo", {bg="#2b2b2b", fg="#51afef"})
vim.api.nvim_set_hl(0, "DiagnosticFloatingHint", {bg="#2b2b2b", fg="#98be65"})
vim.api.nvim_set_hl(0, "DiagnosticSignError", {bg="#2b2b2b", fg="#ec5f67"})
vim.api.nvim_set_hl(0, "DiagnosticSignWarn", {bg="#2b2b2b", fg="#ECBE7B"})
vim.api.nvim_set_hl(0, "DiagnosticSignInfo", {bg="#2b2b2b", fg="#51afef"})
vim.api.nvim_set_hl(0, "DiagnosticSignHint", {bg="#2b2b2b", fg="#98be65"})
vim.api.nvim_set_hl(0, "DiagnosticVirtualTextError", {bg="#2b2b2b", fg="#ec5f67"})
vim.api.nvim_set_hl(0, "DiagnosticVirtualTextWarn", {bg="#2b2b2b", fg="#ECBE7B"})
vim.api.nvim_set_hl(0, "DiagnosticVirtualTextInfo", {bg="#2b2b2b", fg="#51afef"})
vim.api.nvim_set_hl(0, "DiagnosticVirtualTextHint", {bg="#2b2b2b", fg="#98be65"})
vim.api.nvim_set_hl(0, "DiagnosticUnderlineError", {bg="#2b2b2b", fg="#ec5f67"})
vim.api.nvim_set_hl(0, "DiagnosticUnderlineWarn", {bg="#2b2b2b", fg="#ECBE7B"})
vim.api.nvim_set_hl(0, "DiagnosticUnderlineInfo", {bg="#2b2b2b", fg="#51afef"})
vim.api.nvim_set_hl(0, "DiagnosticUnderlineHint", {bg="#2b2b2b", fg="#98be65"})
vim.api.nvim_set_hl(0, "DiagnosticDeprecated", {bg="#2b2b2b", fg="#abb2bf"})
vim.api.nvim_set_hl(0, "DiagnosticUnnecessary", {bg="#2b2b2b", fg="#abb2bf"})

-- LSP HIGHLIGHTS
vim.api.nvim_set_hl(0, "WinhighlightConfig", {bg="#2b2b2b", fg="#ec5f67"})
vim.api.nvim_set_hl(0, "CmpDocumentationBorder", {bg="#2b2b2b", fg="#4b4b4b"})

-- DAP HIGHLIGHTS
vim.api.nvim_set_hl(0, 'DapBreakpoint', {fg='#ec5f67'})
vim.api.nvim_set_hl(0, 'DapLogPoint', {fg='#ecbe7b'})
vim.api.nvim_set_hl(0, 'DapStopped', {fg='#98be65'})
vim.api.nvim_set_hl(0, 'DebugHighlight', {fg='#ec5f67'})

-- MARKED
vim.api.nvim_set_hl(0, 'DapUIScope', {bg='#2b2b2b', fg='#ec5f67'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIType', {bg='#2b2b2b', fg='#ec5f67'}) -- Scope Type 
vim.api.nvim_set_hl(0, 'DapUIDecoration', {bg='#2b2b2b', fg='#4b4b4b'}) 
vim.api.nvim_set_hl(0, 'DapUIModifiedValue', {bg='#2b2b2b', fg='#ecbe7b'}) 

-- UNKNOWN
vim.api.nvim_set_hl(0, 'DapUIFrameName', {bg='#2b2b2b', fg='#737373'}) 
vim.api.nvim_set_hl(0, 'DapUIFloatBorder', {bg='#2b2b2b', fg='#333333'}) 

-- UNDERSTOOD / GREEN
vim.api.nvim_set_hl(0, 'DapUILineNumber', {bg='#2b2b2b', fg='#98be65'}) 
vim.api.nvim_set_hl(0, 'DapUIThread', {bg='#2b2b2b', fg='#98be65'}) 
vim.api.nvim_set_hl(0, 'DapUIStoppedThread', {bg='#2b2b2b', fg='#98be65'}) 
vim.api.nvim_set_hl(0, 'DapUISource', {bg='#2b2b2b', fg='#98be65'}) 

-- MARKED
vim.api.nvim_set_hl(0, 'DapUIWatchesEmpty', {bg='#2b2b2b', fg='#ec5f67'}) 
vim.api.nvim_set_hl(0, 'DapUIWatchesValue', {bg='#2b2b2b', fg='#ec5f67'}) 
vim.api.nvim_set_hl(0, 'DapUIWatchesError', {bg='#2b2b2b', fg='#ec5f67'}) 

-- UNDERSTOOD / YELLOW
vim.api.nvim_set_hl(0, 'DapUIBreakpointsPath', {bg='#2b2b2b', fg='#ecbe7b'}) 
vim.api.nvim_set_hl(0, 'DapUIBreakpointsInfo', {bg='#2b2b2b', fg='#abb2bf'}) 
vim.api.nvim_set_hl(0, 'DapUIBreakpointsCurrentLine', {bg='#2b2b2b', fg='#ecbe7b'}) 
vim.api.nvim_set_hl(0, 'DapUIBreakpointsLine', {bg='#2b2b2b', fg='#ecbe7b'}) 
vim.api.nvim_set_hl(0, 'DapUIBreakpointsDisabledLine', {bg='#2b2b2b', fg='#ecbe7b'}) 

-- MARKED
vim.api.nvim_set_hl(0, 'DapUIStepOver', {bg='#2b2b2b', fg='#ff8800'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIStepBack', {bg='#2b2b2b', fg='#ff8800'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIStepInto', {bg='#2b2b2b', fg='#ecbe7b'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIStepOut', {bg='#2b2b2b', fg='#ecbe7b'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIStop', {bg='#2b2b2b', fg='#ec5f67'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIPlayPause', {bg='#2b2b2b', fg='#98be65'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIRestart', {bg='#2b2b2b', fg='#98be65'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIUnavailable', {bg='#2b2b2b', fg='#ec5f67'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIWinSelect', {bg='#2b2b2b', fg='#4b4b4b'}) -- Scope Title 

-- MARKED
vim.api.nvim_set_hl(0, 'DapUIStepOverNC', {bg='#2b2b2b', fg='#ff8800'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIStepBackNC', {bg='#2b2b2b', fg='#ff8800'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIStepIntoNC', {bg='#2b2b2b', fg='#ecbe7b'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIStepOutNC', {bg='#2b2b2b', fg='#ecbe7b'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIStopNC', {bg='#2b2b2b', fg='#ec5f67'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIPlayPauseNC', {bg='#2b2b2b', fg='#98be65'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIRestartNC', {bg='#2b2b2b', fg='#98be65'}) -- Scope Title 
vim.api.nvim_set_hl(0, 'DapUIUnavailableNC', {bg='#2b2b2b', fg='#4b4b4b'}) -- Scope Title 

-- MASON HIGHLIGHTS
vim.api.nvim_set_hl(0, "MasonMutedBlock", {bg="#4b4b4b", fg="#2b2b2b", bold=true})
vim.api.nvim_set_hl(0, "MasonMuted", {bg="#2b2b2b", fg="#4b4b4b", bold =true})
vim.api.nvim_set_hl(0, "MasonMutedBlockBold", {bg="#4b4b4b", fg="#737373", bold=true})
vim.api.nvim_set_hl(0, "MasonHeader", {bg="#2b2b2b", fg="#2b2b2b"})
vim.api.nvim_set_hl(0, "MasonHeading", {bg="#2b2b2b", fg="#737373", bold=true})
vim.api.nvim_set_hl(0, "MasonHighlightBlockBold", {bg="#737373", fg="#2b2b2b", bold=true})
vim.api.nvim_set_hl(0, "MasonHighlightBlockSecondary", {bg="#2b2b2b", fg="#2b2b2b"})
vim.api.nvim_set_hl(0, "MasonHighlightBlockBoldSecondary", {bg="#2b2b2b", fg="#2b2b2b"})
vim.api.nvim_set_hl(0, "MasonHighlightSecondary", {bg="#2b2b2b", fg="#2b2b2b"})
vim.api.nvim_set_hl(0, "MasonHighlightBlock", {bg="#2b2b2b", fg="#737373"})
vim.api.nvim_set_hl(0, "MasonHeaderSecondary", {bg="#2b2b2b", fg="#2b2b2b"})
vim.api.nvim_set_hl(0, "MasonHighlight", {bg="#2b2b2b", fg="#ec5f67"})

-- HIGHLIGHTS
vim.api.nvim_set_hl(0, "Normal", {bg="#2b2b2b", fg="#737373"})
vim.api.nvim_set_hl(0, "NormalFloat", {bg="#2b2b2b"})
vim.api.nvim_set_hl(0, "FloatBorder", {bg="#2b2b2b", fg="#4b4b4b"})
vim.api.nvim_set_hl(0, "WinSeparator", {bg="#2b2b2b", fg="#4b4b4b"})

-- TELESCOPE HIGHLIGHTS
vim.api.nvim_set_hl(0, "TelescopeNormal", {bg="#2b2b2b", fg="#abb2bf"})
vim.api.nvim_set_hl(0, "TelescopeBorder", {bg="#2b2b2b"})
vim.api.nvim_set_hl(0, "TelescopeSelectionCaret", {bg="#2b2b2b", fg="#ec5f67"})
vim.api.nvim_set_hl(0, "TelescopeSelection", {bg="#2b2b2b", fg="#ec5f67"})
vim.api.nvim_set_hl(0, "TelescopePromptBorder", {bg="#2b2b2b", fg="#4b4b4b"})
vim.api.nvim_set_hl(0, "TelescopeResultsBorder", {bg="#2b2b2b", fg="#4b4b4b"})
vim.api.nvim_set_hl(0, "TelescopePromptPrefix", {bg="#2b2b2b", fg="#ec5f67"})
vim.api.nvim_set_hl(0, "TelescopePromptTitle", {bg="#4b4b4b", fg="#abb2bf", bold=true})
vim.api.nvim_set_hl(0, "TelescopeResultsTitle", {bg="#4b4b4b", fg="#abb2bf", bold=true})
vim.api.nvim_set_hl(0, "TelescopeMatching", {bg="#2b2b2b", fg="#ec5f67"})
vim.api.nvim_set_hl(0, "TelescopePromptCounter", {bg="#2b2b2b", fg="#2b2b2b"})

vim.fn.sign_define('DapBreakpoint', { text ='', texthl='DapBreakpoint' })
vim.fn.sign_define('DapBreakpointCondition', { text='ﳁ', texthl='DapBreakpoint' })
vim.fn.sign_define('DapBreakpointRejected', { text='', texthl='DapBreakpoint' })
vim.fn.sign_define('DapLogPoint', { text='', texthl='DapLogPoint' })
vim.fn.sign_define('DapStopped', { text='', texthl='DapStopped' })
