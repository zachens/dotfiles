local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    -- LSP
    'VonHeikemen/lsp-zero.nvim',
    'neovim/nvim-lspconfig',
    'williamboman/mason.nvim',
    'williamboman/mason-lspconfig.nvim',

    -- CMP
    'hrsh7th/nvim-cmp',
    'saadparwaiz1/cmp_luasnip',
    'hrsh7th/cmp-nvim-lsp-signature-help',
    'hrsh7th/cmp-nvim-lsp',
    'hrsh7th/cmp-buffer',
    'hrsh7th/cmp-path',

    -- Snips
    'rafamadriz/friendly-snippets',
    'L3MON4D3/LuaSnip',
 
    -- Status line
    'nvim-lualine/lualine.nvim',

    -- Rust
    'rust-lang/rust.vim',
    'simrat39/rust-tools.nvim',

    "lukas-reineke/indent-blankline.nvim",
    { "olimorris/onedarkpro.nvim", priority = 1000 },
    { 'windwp/nvim-autopairs', event = "InsertEnter" },

    -- Debug
    { 'mfussenegger/nvim-dap-ui',
    dependencies = {
        'rcarriga/nvim-dap',
    }},

    -- Treesitter
    { 'nvim-treesitter/nvim-treesitter',
    dependencies = {
      'nvim-treesitter/nvim-treesitter-textobjects',
    }},
    build = ':TSUpdate',

    -- Buffers
    { 'ThePrimeagen/harpoon',
    dependencies = {
        'nvim-lua/plenary.nvim',
    }},

    -- Telescope
    { 'nvim-telescope/telescope.nvim', tag = '0.1.2',
    dependencies = {
        'nvim-lua/plenary.nvim',
        { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
    }},
})
