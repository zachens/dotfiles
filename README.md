# Personal Dotfiles

Most of these configs have been designed purely for my system.  
Thus, expect some sort of issues if you're trying to use them.

## Configs

- [Xmonad](https://gitlab.com/zachens/dotfiles/-/tree/master/.config/xmonad)
- [Neovim](https://gitlab.com/zachens/dotfiles/-/tree/master/.config/nvim)
- [Tmux](https://gitlab.com/zachens/dotfiles/-/tree/master/.config/tmux)
- [Bash](https://gitlab.com/zachens/dotfiles/-/tree/master/.config/shell)

## Scripts

- [Shutdown/Restart](https://gitlab.com/zachens/dotfiles/-/tree/master/.local/bin/acpiEvents)
- [Browser Search](https://gitlab.com/zachens/dotfiles/-/tree/master/.local/bin/engineSearch)
- [Screenshots](https://gitlab.com/zachens/dotfiles/-/tree/master/.local/bin/x11Capture)
- [Task Kill](https://gitlab.com/zachens/dotfiles/-/tree/master/.local/bin/pidKill)
